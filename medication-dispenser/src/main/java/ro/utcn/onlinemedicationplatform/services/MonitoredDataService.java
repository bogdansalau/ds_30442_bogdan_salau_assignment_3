package ro.utcn.onlinemedicationplatform.services;

import org.springframework.stereotype.Service;
import ro.utcn.onlinemedicationplatform.entities.MonitoredData;
import ro.utcn.onlinemedicationplatform.repositories.MonitoredDataRepository;

@Service
public class MonitoredDataService {

    private final MonitoredDataRepository monitoredDataRepository;

    private PatientService patientService;

    public MonitoredDataService(MonitoredDataRepository monitoredDataRepository) {
        this.monitoredDataRepository = monitoredDataRepository;
    }

    public Long insert(MonitoredData monitoredData){
        //TODO validator

//        PatientDTO patientDTO = patientService.findPatientById(monitoredData.getPId());
//        Patient patient = PatientBuilder.generateEntityFromDTO(patientDTO);
        return monitoredDataRepository
                .save(monitoredData)
                .getId();
    }
}
