package ro.utcn.onlinemedicationplatform.services;

import org.springframework.stereotype.Service;
import ro.utcn.onlinemedicationplatform.dto.MedicalPlanDTO;
import ro.utcn.onlinemedicationplatform.dto.builders.MedicalPlanBuilder;
import ro.utcn.onlinemedicationplatform.entities.MedicalPlan;
import ro.utcn.onlinemedicationplatform.entities.Patient;
import ro.utcn.onlinemedicationplatform.errorhandler.ResourceNotFoundException;
import ro.utcn.onlinemedicationplatform.repositories.MedicalPlanRepository;

import java.util.Optional;

@Service
public class MedicalPlanService {

    private final MedicalPlanRepository medicalPlanRepository;

    public MedicalPlanService(MedicalPlanRepository medicalPlanRepository) {
        this.medicalPlanRepository = medicalPlanRepository;
    }

    public MedicalPlan findMedicalPlanById(Long id){
        Optional<MedicalPlan> medicalPlan = medicalPlanRepository.findById(id);
        if(!medicalPlan.isPresent()){
            throw new ResourceNotFoundException("MedicalPlan", "medical_plan", id);
        }
        return medicalPlan.get();
    }

    public MedicalPlanDTO findMedicalPlanByPatient(Patient patient){
        Optional<MedicalPlan> medicalPlan = medicalPlanRepository.findByPatient(patient);
        if(!medicalPlan.isPresent()){
            throw new ResourceNotFoundException("MedicalPlan", "medical_plan_patient", patient);
        }
        return new MedicalPlanBuilder().generateDTOFromEntity(medicalPlan.get());
    }

    public Long insertMedicalPlan (MedicalPlanDTO medicalPlanDTO){
        return medicalPlanRepository
                .save(MedicalPlanBuilder.generateEntityFromDTO(medicalPlanDTO))
                .getId();
    }

    public Long update (Long id, MedicalPlan medicalPlan) {
        Optional<MedicalPlan> oldMedicalPlan = medicalPlanRepository.findById(id);
        if(!oldMedicalPlan.isPresent()){
            throw new ResourceNotFoundException("MedicalPlan", "medical_plan", id);
        }
        return medicalPlanRepository
                .save(medicalPlan)
                .getId();
    }

}
