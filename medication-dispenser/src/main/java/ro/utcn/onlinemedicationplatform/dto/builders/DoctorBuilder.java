package ro.utcn.onlinemedicationplatform.dto.builders;

import ro.utcn.onlinemedicationplatform.dto.DoctorDTO;
import ro.utcn.onlinemedicationplatform.entities.Doctor;

public class DoctorBuilder {

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getUsername(),
                doctor.getPassword(),
                doctor.getBirth_date(),
                doctor.getGender(),
                doctor.getAddress()
        );
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO){
        return new Doctor(
                doctorDTO.getName(),
                doctorDTO.getUsername(),
                doctorDTO.getPassword(),
                doctorDTO.getBirthDate(),
                doctorDTO.getGender(),
                doctorDTO.getAddress()
        );
    }

}
