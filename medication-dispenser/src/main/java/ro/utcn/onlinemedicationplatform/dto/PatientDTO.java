package ro.utcn.onlinemedicationplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ro.utcn.onlinemedicationplatform.entities.Caregiver;
import ro.utcn.onlinemedicationplatform.entities.MedicalPlan;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {
    private Long id;
    private String name;
    private String username;
    private String password;
    private String birthDate;
    private Character gender;
    private String address;
    private String medicalRecord;
    private MedicalPlan medicalPlan;
    private Caregiver caregiver;
}
