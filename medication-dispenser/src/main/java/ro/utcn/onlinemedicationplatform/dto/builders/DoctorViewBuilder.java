package ro.utcn.onlinemedicationplatform.dto.builders;

import lombok.NoArgsConstructor;
import ro.utcn.onlinemedicationplatform.dto.view.DoctorViewDTO;
import ro.utcn.onlinemedicationplatform.entities.Doctor;

@NoArgsConstructor
public class DoctorViewBuilder {

    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(
                doctor.getId(),
                doctor.getName()
        );
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO){
        return new Doctor(
                doctorViewDTO.getName()
        );
    }
}
