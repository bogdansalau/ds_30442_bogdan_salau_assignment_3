package ro.utcn.onlinemedicationplatform.dto.builders;

import lombok.NoArgsConstructor;
import ro.utcn.onlinemedicationplatform.dto.view.MedicationViewDTO;
import ro.utcn.onlinemedicationplatform.entities.Medication;

@NoArgsConstructor
public class MedicationViewBuilder {

    public static MedicationViewDTO generateDTOFromEntity(Medication medication){
        return new MedicationViewDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage()
        );
    }

    public static Medication generateEntityFromDTO(MedicationViewDTO medicationViewDTO){
        return new Medication(
                medicationViewDTO.getName(),
                medicationViewDTO.getSideEffects(),
                medicationViewDTO.getDosage()
        );
    }

}
