package ro.utcn.onlinemedicationplatform.dto.builders;

import ro.utcn.onlinemedicationplatform.dto.MonitoredDataDTO;
import ro.utcn.onlinemedicationplatform.entities.MonitoredData;

public class MonitoredDataBuilder {

    public static MonitoredData generateEntityFromDTO(MonitoredDataDTO monitoredDataDTO){
        return new MonitoredData(
                monitoredDataDTO.getStartTime(),
                monitoredDataDTO.getEndTime(),
                monitoredDataDTO.getActivity()
        );
    }

}
