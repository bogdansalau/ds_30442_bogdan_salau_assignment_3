package ro.utcn.onlinemedicationplatform.dto.builders;

import ro.utcn.onlinemedicationplatform.dto.MedicalPlanDTO;
import ro.utcn.onlinemedicationplatform.entities.MedicalPlan;

public class MedicalPlanBuilder {

    public static MedicalPlanDTO generateDTOFromEntity(MedicalPlan medicalPlan){
        return new MedicalPlanDTO(
                medicalPlan.getId(),
                medicalPlan.getName(),
                medicalPlan.getInstructions(),
                medicalPlan.getTreatmentPeriod(),
                medicalPlan.getMedications(),
                medicalPlan.getPatient()
        );
    }

    public static MedicalPlan generateEntityFromDTO(MedicalPlanDTO medicalPlanDTO){
        return new MedicalPlan(
                medicalPlanDTO.getName(),
                medicalPlanDTO.getInstructions(),
                medicalPlanDTO.getTreatmentPeriod(),
                medicalPlanDTO.getPatient(),
                medicalPlanDTO.getMedications()
        );
    }

}
